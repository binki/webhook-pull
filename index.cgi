#!/usr/bin/env node
/* -*- mode: js2 -*- */
'use strict';

const app = require('./app');
const expressAutoserve = require('express-autoserve');

expressAutoserve(app);
