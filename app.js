'use strict';

const _ = require('lodash');
const bodyParser = require('body-parser');
const config = require('./load-config');
const crossSpawn = require('cross-spawn');
const gulpLock = require('gulp-lock');
const path = require('path');
const promisify = require('promisify-node');
const fs = promisify('fs');
const scmp = require('scmp');
const express = require('express');

const app = module.exports = express();

// Only allow up to 4 pull at once.
const globalConcurrencyLock = gulpLock(4);

app.use(bodyParser.json());

const longestKeyLength = _.max(_.map(config.repositories, repository => Buffer.from(repository.key).length));
for (const repository of config.repositories) {
  // Pad and trim to longest length.
  repository.paddedKey = padKey(repository.key);

  // Only allow one pull per repository at a time.
  repository.concurrencyLock = gulpLock(1);
}
function padKey(key) {
  const b = Buffer.alloc(longestKeyLength);
  Buffer.from(String(key) || '').copy(b, 0, 0, longestKeyLength);
  return b;
}

/**
 * @param name The name of the repository to pull.
 *
 * @param key The secret used to authenticate the caller.
 */
function pullRepository(name, key) {
  return Promise.resolve().then(() => {
    // Find repository
    const repository = _.find(config.repositories, r => r.name === name);
    if (!repository) {
      throw new Error(`Repository not found.`);
    }

    // Authenticate
    const keyBuffer = padKey(key);
    if (!scmp(repository.paddedKey, keyBuffer)) {
      throw new Error(`Invalid key.`);
    }

    // Is it a custom command?
    if (repository.command) {
      const executable = repository.command[0];
      const parameters = repository.command.slice(1);
      return new Promise((resolve, reject) => {
        console.log(`spawning command ${executable} ${parameters.join(' ')}`);
        crossSpawn(executable, parameters)
          .on('close', resolve)
          .on('error', reject)
        ;
      });
    }

    // Probe repository.
    const hgPath = path.join(repository.path, '.hg');
    return globalConcurrencyLock.promise(() => repository.concurrencyLock.promise(() => fs.access(hgPath).then(() => true, () => false).then(isMercurial => {
      if (isMercurial) {
        return new Promise((resolve, reject) => {
          crossSpawn('hg', ['-R', repository.path, 'pull', '-u'])
            .on('close', resolve)
            .on('error', reject)
          ;
        });
      }

      const gitPath = path.join(repository.path, '.git');
      return fs.access(gitPath).then(() => true, () => false).then(isGit => {
        if (isGit) {
          return new Promise((resolve, reject) => {
            crossSpawn('git', ['-C', repository.path])
              .on('close', resolve)
              .on('error', reject)
            ;
          });
        }

        throw new Error('Unable to determine type of repository.');
      });
    })));
  });
}

function handleAnyRequest (request, response, next) {
  pullRepository(request.params.repository, request.params.key).then(() => {
    response.writeHead(200, { 'Content-Type': 'text/plain; charset=utf-8' });
    response.end('OK');
  }).catch(next);
};

app.post('/bitbucket/:repository/:key', handleAnyRequest );

app.get('/simple/:repository/:key', handleAnyRequest);
