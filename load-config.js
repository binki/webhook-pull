'use strict';

const _ = require('lodash');
const config = require('./config.json');
const path = require('path');

module.exports = config;

let curDir = __dirname;
while (config.include) {
  const include = path.join(curDir, config.include);
  curDir = path.dirname(include);
  config.include = null;

  const newConfig = require(include);
  // merge repositories
  newConfig.repositories = (config.repositories || []).concat(newConfig.repositories);
  Object.assign(config, newConfig);
}

_.map(config.repositories, repository => {
  if (!repository.path) {
    repository.path = path.join(config.basePath, repository.name);
  }
});
