A basic rate-limited target for something like BitBucket’s
webhooks. Likely unsafe. Assumes that `hg pull` at any time is OK,
which may be acceptable for simple projects.

You probably want to use config.json’s "include" property to specify
a config in a different directory to protect the configuration from
HTTP clients.
